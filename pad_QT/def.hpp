#ifndef DEF_HPP
#define DEF_HPP

#include <QString>

#define _QSS(a) QString::fromStdString(a)
#define _TOSF(i) std::to_string(i).substr(0, std::to_string(i).find(".") + 3)
#define _TOS(i) std::to_string(i)
#define _QN(a) QString::number(a)


#endif // DEF_HPP
