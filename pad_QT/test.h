#ifndef TEST_H
#define TEST_H

#include <QObject>
#include <iostream>
#include <QtTest/QTest>
#include "TravelAgency.h"

class test : public QObject
{
    Q_OBJECT
public:
    explicit test(QObject *parent = nullptr);
    ~test();

private slots:
    void testRentAvis();
    void testFlightUnited();
    void testBookingsOfThousand();

private:
    TravelAgency* ta = new TravelAgency();

public slots:
};

#endif // TEST_H
