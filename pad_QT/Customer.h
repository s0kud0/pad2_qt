#pragma once
#include "Travel.h"

class Customer
{
private:
	long id;
	string name;
	vector<Travel*> travelList;
public:
	Customer(long id, string name);
	virtual ~Customer();

	void addTravel(Travel* travel);
	long GetId();
    string GetName();
    int getTravelCount();
};

