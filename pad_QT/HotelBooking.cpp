#include "HotelBooking.h"


HotelBooking::HotelBooking(long id, double price, long travelId, string fromDate, string toDate, string hotel, string town, bool smoke)
    : Booking(id, price, travelId, fromDate, toDate), hotel(hotel), town(town), smoke(smoke)
{
}

HotelBooking::~HotelBooking()
{
}

string HotelBooking::Hotel()
{
    return this->hotel;
}

string HotelBooking::Town()
{
    return this->town;
}

string HotelBooking::showDetails()
{
    return this->smoke? "Raucherzimmer" : "Nichtraucherzimmer";
}
