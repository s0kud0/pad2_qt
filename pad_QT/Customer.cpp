#include "Customer.h"



Customer::Customer(long id, string name)
    : id(id), name(name)
{
}


Customer::~Customer()
{
}

void Customer::addTravel(Travel * travel)
{
	travelList.push_back(travel);
}

long Customer::GetId()
{
    return id;
}

string Customer::GetName()
{
    return name;
}

int Customer::getTravelCount()
{
    return this->travelList.size();
}
