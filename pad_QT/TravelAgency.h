#pragma once
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <map>
#include "Booking.h"
#include "FlightBooking.h"
#include "HotelBooking.h"
#include "RentalCarReservation.h"
#include "Travel.h"
#include "Customer.h"
#include "Linkedlist.h"
#include "airport.h"

using namespace std;

class TravelAgency
{
public:
    //vector<Booking*> allBookings;
    SortedList<Booking> allBookings = SortedList<Booking>([](Booking* a, Booking* b) { return a->GetTravelId() < b->GetTravelId();});
	vector<Customer*> allCustomers;
	vector<Travel*> allTravels;

    vector<Airport*> airports;

    double price = 0;
    int numflights = 0;
    int numhotels = 0;
    int numcars = 0;
    int numCustomers = 0;
    int numTravels = 0;

	vector<string> split(const string &s, char delimiter);

public:
	TravelAgency();
	virtual ~TravelAgency();
	
    int ReadFile();
	Booking* findBooking(long id);
	Travel* findTravel(long id);
    Customer* findCustomer(long id);

    string SearchAirport(string token);

    std::map<int, char> CheckTravels();

    void Print();
};

