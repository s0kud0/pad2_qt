#ifndef AIRPORT_H
#define AIRPORT_H

#include <string>

class Airport
{
private:
    std::string name;
    std::string token;
    float longitude;
    float latitude;

public:
    Airport(std::string token, std::string name, float longitude, float latitude);

    bool operator==(std::string token);

    float GetLat();
    float GetLong();
    std::string GetName();
    std::string GetToken();
};

#endif // AIRPORT_H
