#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->ui->preis->setMaximum(100000000.000000);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionDatei_einlesen_triggered()
{
    int err = 0;
    if ((err = this->travelAgency.ReadFile()))
    {
        if (err == -1)
        {
            QMessageBox::information(nullptr, "Fehler", "Datei konnte nicht gelesen werden", QMessageBox::StandardButton::Ok);
        }
        else
        {
            QMessageBox::information(nullptr, "Fehler", "Fehlendes Attribut in Zeile " + _QN(err), QMessageBox::StandardButton::Ok);
        }
    }
    else
    {
        std::string result = "Es wurden " + _TOS(this->travelAgency.allBookings.Size()) + " Buchungen, " + _TOS(this->travelAgency.numCustomers) + " Kunden und " + _TOS(this->travelAgency.numTravels) + " Reisen im Gesamtwert von " + _TOSF(this->travelAgency.price) + " eingelesen.";
        QMessageBox::information(nullptr, "Einleseergebnis", _QSS(result), QMessageBox::StandardButton::Ok);
    }
}

void MainWindow::on_actionBuchungen_anzeigen_triggered()
{
    this->ui->tableWidget->clear();
    this->ui->tableWidget->setRowCount(this->travelAgency.allBookings.Size());
    this->ui->tableWidget->setColumnCount(3);

    this->ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Buchungsnummer"));
    this->ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Preis"));
    this->ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Kunde"));
    this->travelAgency.allBookings.setCurrentToStart();

    for(size_t i = 0; i < this->travelAgency.allBookings.Size(); i++) {
        auto b = this->travelAgency.allBookings.getCurrent();
        this->travelAgency.allBookings.Next();
        //table
        auto id = new QTableWidgetItem(_QN(b->GetId()));
        auto price = new QTableWidgetItem(_QN(b->GetPrice()));
        auto name = new QTableWidgetItem(_QSS(this->travelAgency.findCustomer(this->travelAgency.findTravel(b->GetTravelId())->GetCustomerId())->GetName()));
        this->ui->tableWidget->setItem(i, 0, id);
        this->ui->tableWidget->setItem(i, 1, price);
        this->ui->tableWidget->setItem(i, 2, name);
    }
}

void MainWindow::on_tableWidget_cellClicked(int row, int column)
{
    auto booking = this->travelAgency.allBookings.at(row);
    this->ui->calendarWidget_2->setSelectedDate(QDate::fromString(_QSS(booking->GetFromDate()), "yyyyMMdd"));
    this->ui->calendarWidget->setSelectedDate(QDate::fromString(_QSS(booking->GetToDate()), "yyyyMMdd"));
    this->ui->lineEdit->setText(_QSS(_TOS(booking->GetId())));
    Customer* c = this->travelAgency.findCustomer(this->travelAgency.findTravel(booking->GetTravelId())->GetCustomerId());
    this->ui->kunde->setText(_QSS(c->GetName()));
    this->ui->preis->setValue(booking->GetPrice());
    this->ui->reise->setValue(booking->GetTravelId());

    const char* n = typeid(*booking).name();
    if (!strcmp(n,"20RentalCarReservation"))
    {
        RentalCarReservation* r = reinterpret_cast<RentalCarReservation*>(booking);
        this->ui->stackedWidget->setCurrentIndex(0);
        this->ui->a_abholstation->setText(_QSS(r->PickupLocation()));
        this->ui->a_rueckgabestation->setText(_QSS(r->ReturnLocation()));
        this->ui->a_vermietung->setText(_QSS(r->Company()));
        this->ui->a_versicherung->setText(_QSS(r->showDetails()));
    }
    else if (!strcmp(n, "13FlightBooking"))
    {
        FlightBooking* f = reinterpret_cast<FlightBooking*>(booking);
        this->ui->stackedWidget->setCurrentIndex(1);
        this->ui->f_start->setText(_QSS(this->travelAgency.SearchAirport(f->FromDest())));
        this->ui->f_ziel->setText(_QSS(this->travelAgency.SearchAirport(f->ToDest())));
        this->ui->f_sitz->setText(_QSS(booking->showDetails()));
    }
    else if (!strcmp(n,"12HotelBooking"))
    {
        HotelBooking* h = reinterpret_cast<HotelBooking*>(booking);
        this->ui->stackedWidget->setCurrentIndex(2);
        this->ui->h_hotel->setText(_QSS(h->Hotel()));
        this->ui->h_stadt->setText(_QSS(h->Town()));
        this->ui->h_smoke->setChecked(h->showDetails() == "Raucherzimmer");
    }
}

void MainWindow::on_actionProgramm_beenden_triggered()
{
    QCoreApplication::exit(0);
}

void MainWindow::on_actionFehler_berpr_fen_triggered()
{
    std::map<int, char> check = this->travelAgency.CheckTravels();

    checkDialog* window = new checkDialog();

    window->setData(check);

    window->exec();

    delete window;

}
