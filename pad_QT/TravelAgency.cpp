#include "TravelAgency.h"

TravelAgency::TravelAgency()
{
}


TravelAgency::~TravelAgency()
{	
    allBookings.~SortedList();
}


int TravelAgency::ReadFile()
{
    fstream fileStream;
    fileStream.open("bookings_praktikum5.txt", fstream::in);
    if (!fileStream.is_open())
    {
        cout << "Unable to open file" << endl;
        return -1;
    }

    vector<string> bookings;

    while (!fileStream.eof())
    {
        string currentLine;
        getline(fileStream, currentLine);
        bookings.push_back(currentLine);
    }
    fileStream.close();
    for (size_t i = 0; i < bookings.size(); i++)
    {
        try
        {
            vector<string> tokens = split(bookings[i], '|');

            Customer* currentCustomer = nullptr;
            Travel* currentTravel = nullptr;

            if ((currentCustomer = findCustomer(stol(tokens.at(6)))) == nullptr)
            {
                currentCustomer = new Customer(stol(tokens.at(6)), tokens.at(7));
                allCustomers.push_back(currentCustomer);
                ++numCustomers;
            }

            if ((currentTravel = findTravel(stol(tokens.at(5)))) == nullptr)
            {
                currentTravel = new Travel(stol(tokens.at(5)), stol(tokens[6]));
                allTravels.push_back(currentTravel);
                currentCustomer->addTravel(currentTravel);
                ++numTravels;
            }

            if (tokens.at(0) == "F")
            {
                FlightBooking* flight = new FlightBooking(stol(tokens.at(1)), stod(tokens.at(2)), stol(tokens.at(5)), tokens.at(3), tokens.at(4), tokens.at(8), tokens.at(9), tokens.at(10), tokens.at(11)[0]);
                allBookings.insert(flight);
                TravelDependency tmp;
                tmp.BookingID = stol(tokens.at(1));
                if (tokens.size() >= 13)
                {
                    tmp.Dep1 = stol(tokens.at(12));
                }
                if (tokens.size() >= 14)
                {
                    tmp.Dep2 = stol(tokens.at(13));
                }
                currentTravel->addBooking(flight, tmp);
                price += stod(tokens.at(2));
                ++numflights;
            }
            else if (tokens.at(0) == "H")
            { //Kommentiert
                HotelBooking* hotel = new HotelBooking(stol(tokens.at(1)), stod(tokens.at(2)), stol(tokens.at(5)), tokens.at(3), tokens.at(4), tokens.at(8), tokens.at(9), atoi(tokens.at(10).c_str()) == 1);
                allBookings.insert(hotel);
                TravelDependency tmp;
                tmp.BookingID = stol(tokens.at(1));
                if (tokens.size() >= 12)
                {
                    tmp.Dep1 = stol(tokens.at(11));
                }
                if (tokens.size() >= 13)
                {
                    tmp.Dep2 = stol(tokens.at(12));
                }
                currentTravel->addBooking(hotel, tmp);
                price += stod(tokens.at(2));
                ++numhotels;
            }
            else
            {
                RentalCarReservation* car = new RentalCarReservation(stol(tokens.at(1)), stod(tokens.at(2)), stol(tokens.at(5)), tokens.at(3), tokens.at(4), tokens.at(8), tokens.at(9), tokens.at(10), tokens.at(11));
                allBookings.insert(car);
                TravelDependency tmp;
                tmp.BookingID = stol(tokens.at(1));
                if (tokens.size() >= 13)
                {
                    tmp.Dep1 = stol(tokens.at(12));
                }
                if (tokens.size() >= 14)
                {
                    tmp.Dep2 = stol(tokens.at(13));
                }
                currentTravel->addBooking(car, tmp);
                price += stod(tokens.at(2));
                ++numcars;
            }
        }
        catch(exception e)
        {
            numcars = 0;
            numflights = 0;
            numCustomers = 0;
            numhotels = 0;
            numTravels = 0;
            while(allBookings.Size() > 0)
            {
                allBookings.deleteCurrent();
            }
            return i + 1;
        }
    }

    for(auto travel:allTravels)
    {
        travel->SetDependencyArcs();
    }

    try
    {
        auto b = allBookings[1000];
    }
    catch(exception* e)
    {
        std::cout << e->what() << std::endl;
    }

    //##########################################    Airports    ##################################################

    fileStream.open("airports.txt", fstream::in);
    if (!fileStream.is_open())
    {
        cout << "Unable to open airports.txt";
        return -2;
    }

    vector<string> ports;

    while (!fileStream.eof())
    {
        string currentLine;
        getline(fileStream, currentLine);
        ports.push_back(currentLine);
    }
    fileStream.close();

    for(auto port : ports)
    {
        vector<string> values = split(port, '|');
        Airport* airport = new Airport(values.at(0), values.at(1), stof(values.at(2)), stof(values.at(3)));
        this->airports.push_back(airport);
    }

    //this->Print();
    return 0;
}

Booking * TravelAgency::findBooking(long id)
{
    for (size_t i = 0; i < allBookings.Size(); i++)
    {
        if (allBookings.at(i)->GetId() == id)
        {
            return allBookings.at(i);
        }
    }
    return nullptr;
}

Travel * TravelAgency::findTravel(long id)
{
    for (size_t i = 0; i < allTravels.size(); i++)
    {
        if (allTravels[i]->GetId() == id)
        {
            return allTravels[i];
        }
    }
    return nullptr;
}

Customer * TravelAgency::findCustomer(long id)
{
    for (size_t i = 0; i < allCustomers.size(); i++)
    {
        if (allCustomers[i]->GetId() == id)
        {
            return allCustomers[i];
        }
    }
    return nullptr;
}

string TravelAgency::SearchAirport(string token)
{
    for(auto airport : airports)
    {
        if (*airport == token)
        {
            return airport->GetName();
        }
    }
    return "";
}

std::map<int, char> TravelAgency::CheckTravels()
{
    std::map<int, char> ret;

    for(auto travel:allTravels)
    {
        char val = 0;
        val = travel->CheckRoundTrip()? 0 : 8;
        val |= travel->CheckMissingHotel()? 0 : 4;
        val |= travel->CheckNeedlessHotel()? 0 : 2;
        val |= travel->CheckNeedLessRentalCar()? 0 : 1;
        ret.insert(make_pair(travel->GetId(), val));
    }
    return ret;
}

void TravelAgency::Print()
{
    cout << "Es wurden " << numflights << " Flugreservierungen, " << numhotels << " Hotelbuchungen und " << numcars << " Mietwagenreservierungen im Wert von " << price << " Euro eingelesen." << endl;
    cout << "Es wurden " << numTravels << " Reisen und " << numCustomers << " Kunden angelegt." << endl;
    cout << "Der Kunde mit der ID 1 hat " << this->findCustomer(1)->getTravelCount() << " Reisen Gebucht." << endl;
    cout << "Zur Reise mit der ID 17 gehören " << this->findTravel(17)->getBookingCount() << " Buchungen" << endl;
}

//Splits a string like string.split function in C#
// Parameter &s = String to split
// Parameter &delimiter = char to split at
vector<string> TravelAgency::split(const string &s, char delimiter)
{
    vector<string> elements;
    stringstream ss(s);
    string item;
    while (getline(ss, item, delimiter))
    {
        elements.push_back(item);
    }

    return elements;
}
