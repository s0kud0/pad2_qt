#pragma once
#include "Booking.h"
#include <string>

using namespace std;

class FlightBooking : public Booking
{
private:
	string fromDest;
	string toDest;
	string airline;
    char seatPref;
public:
    FlightBooking(long id, double price, long travelId, string fromDate, string toDate, string fromDest, string toDest, string airline, char seatPref);
	virtual ~FlightBooking();

    string FromDest();
    string ToDest();
    string Airline();

    virtual string showDetails() override final;
};

