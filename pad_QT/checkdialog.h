#ifndef CHECKDIALOG_H
#define CHECKDIALOG_H

#include <QDialog>
#include <map>
#include "def.hpp"

namespace Ui {
class checkDialog;
}

class checkDialog : public QDialog
{
    Q_OBJECT

public:
    explicit checkDialog(QWidget *parent = 0);
    ~checkDialog();

    void setData(std::map<int, char>& data);


private:
    Ui::checkDialog *ui;
};

#endif // CHECKDIALOG_H
