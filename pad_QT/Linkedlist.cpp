#ifndef LINKEDLIST_CPP
#define LINKEDLIST_CPP
#include "Linkedlist.h"

template<class T>
LinkedList<T>::~LinkedList()
{
    while (!this->Empty())
    {
        this->deleteCurrent();
    }
}

template<class T>
void LinkedList<T>::insert(T *data)
{
    if (this->size == 0)
    {
        this->start = this->end = this->current = new Node<T>(data);
    }
    else
    {
        Node<T>* tmp = new Node<T>(data);
        if (this->current == this->start)
        {
            this->start = tmp;
            tmp->next = this->current;
            this->current->prev = tmp;
        }
        else
        {
            tmp->next = this->current;
            tmp->prev = this->current->prev;
            tmp->prev->next = tmp;
            this->current->prev = tmp;
        }
    }
    ++this->size;
}

template<class T>
void LinkedList<T>::insert_after(T *data)
{
    if (this->size == 0)
    {
        this->start = this->end = this->current = new Node<T>(data);
    }
    else
    {
        Node<T>* tmp = new Node<T>(data);
        if (this->current == this->end)
        {
            this->end = tmp;
            tmp->prev = this->current;
            this->current->next = tmp;
        }
        else
        {
            tmp->prev = this->current;
            tmp->next = this->current->next;
            tmp->next->prev = tmp;
            this->current->next = tmp;
        }
    }
    ++this->size;
}

template<class T>
T *LinkedList<T>::deleteCurrent()
{
    T* ret = nullptr;
    if (this->size == 0) return ret;
    if (this->current == this->start)
    {
        this->start = this->current->next;
        ret = this->current->data;
        delete this->current;
        this->current = this->start;
    }
    else if (this->current == this->end)
    {
        this->end = this->current->prev;
        ret = this->current->data;
        delete this->current;
        this->current = this->end;
    }
    else
    {
        auto tmp = this->current->next->prev = this->current->prev;
        this->current->prev->next = this->current->next;
        ret = this->current->data;
        delete this->current;
        this->current = tmp;
    }
    --this->size;
    return ret;
}

template<class T>
void LinkedList<T>::setCurrent(size_t idx)
{
    this->current = this[idx];
}

template<class T>
void LinkedList<T>::setCurrentToStart()
{
    this->current = this->start;
}

template<class T>
void LinkedList<T>::setCurrentToEnd()
{
    this->current = this->end;
}

template<class T>
T* LinkedList<T>::getCurrent()
{
    return this->current->data;
}

template<class T>
void LinkedList<T>::Next()
{
    if (this->current != this->end)
    {
        this->current = this->current->next;
    }
}

template<class T>
void LinkedList<T>::Prev()
{
    if (this->current != this->start)
    {
        this->current = this->current->prev;
    }
}

template<class T>
bool LinkedList<T>::Empty()
{
    return this->size == 0;
}

template<class T>
size_t LinkedList<T>::Size()
{
    return this->size;
}

template<class T>
T *LinkedList<T>::at(size_t idx)
{
    if (idx >= this->size)
    {
        throw new IndexOutOfRangeException;
    }
    Node<T>* tmp;
    if (idx > this->size / 2) //search from end
    {
        tmp = this->end;
        for (size_t i = this->size - 1; i > idx; --i)
        {
            tmp = tmp->prev;
        }
    }
    else //search from start
    {
        tmp = this->start;
        for (size_t i = 0; i < idx; ++i)
        {
            tmp = tmp->next;
        }
    }
    return tmp->data;
}

template<class T>
T *LinkedList<T>::operator[](size_t idx)
{
    return this->at(idx);
}

//######################################  Sorted List   #########################################

template<class T>
void SortedList<T>::insert(T *data)
{
    this->current = this->start;

    if (this->size == 0)
    {
        this->LinkedList<T>::insert(data);
    }
    else if (this->size == 1)
    {
        if (this->cmp(this->current->data, data))
        {
            this->insert_after(data);
        }
        else
        {
            this->LinkedList<T>::insert(data);
        }
    }
    else
    {
        int tmpSize = static_cast<int>(this->size);
        for (int i = 0; i < tmpSize; i++)
        {
            if (!this->cmp(this->current->data, data))
            {
                this->LinkedList<T>::insert(data);
                break;
            }
            if (this->current == this->end)
            {
                this->LinkedList<T>::insert_after(data);
                break;
            }
            this->current = this->current->next;
        }
    }
}
#endif
