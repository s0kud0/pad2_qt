#-------------------------------------------------
#
# Project created by QtCreator 2018-05-04T12:50:44
#
#-------------------------------------------------

QT       += core gui testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pad_QT
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    Booking.cpp \
    Customer.cpp \
    FlightBooking.cpp \
    HotelBooking.cpp \
    RentalCarReservation.cpp \
    Travel.cpp \
    TravelAgency.cpp \
    test.cpp \
    Linkedlist.cpp \
    checkdialog.cpp \
    airport.cpp

HEADERS += \
        mainwindow.h \
    Booking.h \
    Customer.h \
    FlightBooking.h \
    HotelBooking.h \
    RentalCarReservation.h \
    Travel.h \
    TravelAgency.h \
    def.hpp \
    test.h \
    Linkedlist.h \
    graph.hpp \
    checkdialog.h \
    airport.h

FORMS += \
        mainwindow.ui \
    checkdialog.ui

DISTFILES += \
    bookings.txt \
    bookings2.txt
