#include "airport.h"

Airport::Airport(std::string token, std::string name, float longitude, float latitude)
    : token(token), name(name), longitude(longitude), latitude(latitude)
{

}

bool Airport::operator==(std::string token)
{
    return this->token == token;
}

float Airport::GetLat()
{
    return this->latitude;
}

float Airport::GetLong()
{
    return this->longitude;
}

std::string Airport::GetName()
{
    return this->name;
}

std::string Airport::GetToken()
{
    return this->token;
}
