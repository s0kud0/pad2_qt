#pragma once
#include "Booking.h"
#include <string>

using namespace std;

class HotelBooking : public Booking
{
private:
	string hotel;
	string town;
    bool smoke;
public:
    HotelBooking(long id, double price, long travelId, string fromDate, string toDate, string hotel, string town, bool smoke);
	virtual ~HotelBooking();

    string Hotel();
    string Town();

    string showDetails() override final;
};

