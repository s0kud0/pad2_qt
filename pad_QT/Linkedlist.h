
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <functional>
#include <exception>

class IndexOutOfRangeException : public std::exception
{
    virtual const char* what() const throw()
    {
        return "Index was out of RA(n)ge (2) (gibbet bald für PS4, XB1, PC)";
    }
};

template <class T>
class LinkedList
{
private:
    template <class TN>
    class Node
    {
    public:
        Node<TN>* prev;
        Node<TN>* next;
        TN* data;

        Node(TN* data) : data(data)
        {}
        ~Node()
        {
            delete data;
        }
    };

protected:
    Node<T>* start;
    Node<T>* end;
    Node<T>* current;
    size_t size;

public:
    LinkedList() : start(nullptr), end(nullptr), current(nullptr), size(0) {}

    virtual ~LinkedList();

    virtual void insert(T* data);

    virtual void insert_after(T* data);

    T* deleteCurrent();

    void setCurrent(size_t idx);

    inline void setCurrentToStart();

    inline void setCurrentToEnd();

    inline T* getCurrent();

    inline void Next();

    inline void Prev();

    inline bool Empty();

    inline size_t Size();

    T* at(size_t idx);

    T* operator[](size_t idx);

};

template <class T>
class SortedList : public LinkedList<T>
{
private:
    std::function<bool(T* a, T* b)> cmp;
public:
    SortedList(std::function<bool(T* a, T* b)> cmp) : LinkedList<T>(), cmp(cmp) {}
    virtual ~SortedList() {}
    virtual void insert(T* data) override;
};

#include "LinkedList.cpp"

#endif // LINKEDLIST_H
