#include "RentalCarReservation.h"


RentalCarReservation::RentalCarReservation(long id, double price, long travelId, string fromDate, string toDate, string pickupLocation, string returnLocation, string company, string insuranceType)
    : Booking(id, price, travelId, fromDate, toDate), pickupLocation(pickupLocation), returnLocation(returnLocation), company(company), insuranceType(insuranceType)
{
}

RentalCarReservation::~RentalCarReservation()
{
}

string RentalCarReservation::PickupLocation()
{
    return this->pickupLocation;
}

string RentalCarReservation::ReturnLocation()
{
    return this->returnLocation;
}

string RentalCarReservation::Company()
{
    return this->company;
}

string RentalCarReservation::showDetails()
{
    return this->insuranceType;
}
