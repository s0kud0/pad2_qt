#include "test.h"


test::test(QObject *parent) : QObject(parent)
{
    ta->ReadFile();
}

test::~test()
{
    delete ta;
    ta = nullptr;
}

void test::testRentAvis()
{
    int countAvis = 0;
    for(size_t i = 0; i < ta->allBookings.Size(); i++)
    {
        auto b = ta->allBookings.at(i);
        if (!strcmp(typeid(*b).name(),"20RentalCarReservation"))
        {
            if (reinterpret_cast<RentalCarReservation*>(b)->Company() == "Avis")
            {
                ++countAvis;
            }
        }
    }
    QVERIFY(countAvis == 5);
}

void test::testFlightUnited()
{
    int count = 0;
    for(size_t i = 0; i < ta->allBookings.Size(); i++)
    {
        auto b = ta->allBookings.at(i);
        if (!strcmp(typeid(*b).name(), "13FlightBooking"))
        {
            if (reinterpret_cast<FlightBooking*>(b)->Airline() == "United Airlines")
            {
                ++count;
            }
        }
    }
    QVERIFY(count == 3);
}

void test::testBookingsOfThousand()
{
    int count = 0;
    for(size_t i = 0; i < ta->allBookings.Size(); i++)
    {
        auto b = ta->allBookings.at(i);
        if (b->GetPrice() >= 1000)
        {
            ++count;
        }
    }
    QVERIFY(count == 31);
}
