#include "Travel.h"



Travel::Travel(long id, long customerId)
    : id(id), customerId(customerId)
{
}


Travel::~Travel()
{
    delete this->sl;
}

void Travel::SetDependencyArcs()
{
    for(auto dep:dependencies)
    {
        for(int i = 0; i < 20; i++)
        {
            if (this->bookings->getVertexValue(i))
            {
                if (this->bookings->getVertexValue(i)->GetId() == dep.BookingID)
                    dep.BookingID = i;
                if (dep.Dep1 != -1 && this->bookings->getVertexValue(i)->GetId() == dep.Dep1)
                {
                    dep.Dep1 = i;
                }
                if (dep.Dep2 != -1 && this->bookings->getVertexValue(i)->GetId() == dep.Dep2)
                {
                    dep.Dep2 = i;
                }
            }
        }
        if (dep.Dep1 != -1)
        {
            this->bookings->insertArc(dep.BookingID, dep.Dep1);
        }
        if (dep.Dep2 != -1)
        {
            this->bookings->insertArc(dep.BookingID, dep.Dep2);
        }
    }

    DepthFirstSearch(*this->bookings);


    for (size_t i = 0; i < this->travelBooking.size(); i++) {
        node_data* node = new node_data;
        node->i = i;
        node->bezeichner = this->bookings->getVertexValue(i);
        node->end = this->bookings->getEnd(i);
        this->sl->insert(node);
    }
}

bool Travel::CheckRoundTrip()
{
    std::string start;
    this->sl->setCurrentToStart();
    Booking* b = this->sl->getCurrent()->bezeichner;
    this->sl->Next();
    if (auto f = dynamic_cast<FlightBooking*>(b))
    {
        start = f->FromDest();
    }
    this->sl->setCurrentToEnd();
    b = this->sl->getCurrent()->bezeichner;
    if(auto f = dynamic_cast<FlightBooking*>(b))
    {
        if (f->ToDest() == start)
        {
            return true;
        }
    }
    return false;
}

bool Travel::CheckMissingHotel()
{
    std::string currentFlightDate;
    std::string currentHotelDate;
    this->sl->setCurrentToStart();
    for (size_t i = 0; i < this->sl->Size(); i++)
    {
        Booking* booking = this->sl->getCurrent()->bezeichner;
        if (auto f = dynamic_cast<FlightBooking*>(booking))
        {
            currentFlightDate = f->GetToDate();
            if (currentHotelDate != "")
            {
                if (stol(currentHotelDate) < stol(f->GetFromDate()))
                {
                    return false;
                }
                currentHotelDate = "";
            }
        }
        else if (auto h = dynamic_cast<HotelBooking*>(booking))
        {
            currentHotelDate = h->GetToDate();
            if (currentFlightDate != "")
            {
                if (stol(currentFlightDate) < stol(h->GetFromDate()))
                {
                    return false;
                }
                currentFlightDate = "";
            }
        }
        this->sl->Next();
    }
    return true;
}

bool Travel::CheckNeedlessHotel()
{
    std::string currentFlightDate;
    std::string currentHotelDate;
    this->sl->setCurrentToStart();
    for (size_t i = 0; i < this->sl->Size(); i++)
    {
        Booking* booking = this->sl->getCurrent()->bezeichner;
        if (auto f = dynamic_cast<FlightBooking*>(booking))
        {
            currentFlightDate = f->GetToDate();
            if (currentHotelDate != "")
            {
                if (stol(currentHotelDate) > stol(f->GetFromDate()))
                {
                    return false;
                }
                currentHotelDate = "";
            }
        }
        else if (auto h = dynamic_cast<HotelBooking*>(booking))
        {
            currentHotelDate = h->GetToDate();
            if (currentFlightDate != "")
            {
                if (stol(currentFlightDate) > stol(h->GetFromDate()))
                {
                    return false;
                }
                currentFlightDate = "";
            }
        }
        this->sl->Next();
    }
    return true;
}

bool Travel::CheckNeedLessRentalCar()
{
    std::string currentFlightDate;
    std::string currentRentalCarDate;
    this->sl->setCurrentToStart();
    for (int i = 0; i < 20; i++)
    {
        Booking* booking = this->sl->getCurrent()->bezeichner;
        if (auto f = dynamic_cast<FlightBooking*>(booking))
        {
            currentFlightDate = f->GetToDate();
            if (currentRentalCarDate != "")
            {
                if (stol(currentRentalCarDate) > stol(f->GetFromDate()))
                {
                    return false;
                }
                currentRentalCarDate = "";
            }
        }
        else if (auto r = dynamic_cast<RentalCarReservation*>(booking))
        {
            currentRentalCarDate = r->GetToDate();
            if (currentFlightDate != "")
            {
                if (stol(currentFlightDate) > stol(r->GetFromDate()))
                {
                    return false;
                }
                currentFlightDate = "";
            }
        }
        this->sl->Next();
    }
    return true;
}

long Travel::GetId()
{
    return id;
}

long Travel::GetCustomerId()
{
    return customerId;
}

void Travel::addBooking(Booking * booking, TravelDependency dep)
{
    travelBooking.push_back(booking);
    bookings->insertVertex(index++, booking);
    this->dependencies.push_back(dep);
}

int Travel::getBookingCount()
{
    return this->travelBooking.size();
}
