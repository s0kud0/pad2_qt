#include "checkdialog.h"
#include "ui_checkdialog.h"

checkDialog::checkDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::checkDialog)
{
    ui->setupUi(this);
}



checkDialog::~checkDialog()
{
    delete ui;
}

void checkDialog::setData(std::map<int, char>& data)
{
    this->ui->tableWidget->clear();
    this->ui->tableWidget->setRowCount(data.size());
    this->ui->tableWidget->setColumnCount(5);

    this->ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Reisenummer"));
    this->ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Rundreise?"));
    this->ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Fehlendes Hotel?"));
    this->ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Ueberfluessiges Hotel?"));
    this->ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Ueberfluessiger Mietwagen?"));
    int c = 0;
    for(std::map<int, char>::iterator it = data.begin(); it != data.end(); it++)
    {
        this->ui->tableWidget->setItem(c, 0, new QTableWidgetItem(_QN(it->first)));
        this->ui->tableWidget->setItem(c, 1, new QTableWidgetItem(it->second & 8? "Nein" : "Ja"));
        this->ui->tableWidget->item(c, 1)->setBackground(it->second & 8? Qt::red : Qt::green);
        this->ui->tableWidget->setItem(c, 2, new QTableWidgetItem(it->second & 4? "Ja" : "Nein"));
        this->ui->tableWidget->item(c, 2)->setBackground(it->second & 4? Qt::red : Qt::green);
        this->ui->tableWidget->setItem(c, 3, new QTableWidgetItem(it->second & 2? "Ja" : "Nein"));
        this->ui->tableWidget->item(c, 3)->setBackground(it->second & 2? Qt::red : Qt::green);
        this->ui->tableWidget->setItem(c, 4, new QTableWidgetItem(it->second & 1? "Ja" : "Nein"));
        this->ui->tableWidget->item(c, 4)->setBackground(it->second & 1? Qt::red : Qt::green);
        ++c;
    }
}
