#pragma once
#include <vector>
#include <typeinfo>
#include "Booking.h"
#include "FlightBooking.h"
#include "HotelBooking.h"
#include "RentalCarReservation.h"
#include "graph.hpp"
#include "Linkedlist.h"

struct TravelDependency
{
    int BookingID;
    int Dep1 = -1;
    int Dep2 = -1;
};

struct node_data
{
    int i;
    Booking* bezeichner;
    int end;
};

class Travel
{
private: 
    long id;
    long customerId;
    vector<Booking*> travelBooking;
    vector<TravelDependency> dependencies;
    Graph<Booking*,20>* bookings = new Graph<Booking*, 20>();
    SortedList<node_data>* sl = new SortedList<node_data>([](node_data* a, node_data* b) { return a->end < b->end;});
    int index = 0;
public:
    Travel(long id, long customerId);
    virtual ~Travel();

    void SetDependencyArcs();
    bool CheckRoundTrip();
    bool CheckMissingHotel();
    bool CheckNeedlessHotel();
    bool CheckNeedLessRentalCar();
    long GetId();
    long GetCustomerId();
    void addBooking(Booking* booking, TravelDependency dep);
    int getBookingCount();
};

