#include "Booking.h"



Booking::Booking(long id, double price, long travelId, string fromDate, string toDate)
    : id(id), price(price), travelId(travelId), fromDate(fromDate), toDate(toDate)
{
}


Booking::~Booking()
{
}

long Booking::GetId()
{
    return id;
}

double Booking::GetPrice()
{
    return price;
}

long Booking::GetTravelId()
{
    return travelId;
}

string Booking::GetFromDate()
{
    return fromDate;
}

string Booking::GetToDate()
{
    return toDate;
}
