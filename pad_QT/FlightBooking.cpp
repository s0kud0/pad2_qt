#include "FlightBooking.h"


FlightBooking::FlightBooking(long id, double price, long travelId, string fromDate, string toDate, string fromDest, string toDest, string airline, char seatPref)
    : Booking(id, price, travelId, fromDate, toDate), fromDest(fromDest), toDest(toDest), airline(airline), seatPref(seatPref)
{
}

FlightBooking::~FlightBooking()
{
}

string FlightBooking::FromDest()
{
    return this->fromDest;
}

string FlightBooking::ToDest()
{
    return this->toDest;
}

string FlightBooking::Airline()
{
    return this->airline;
}

string FlightBooking::showDetails()
{
    return this->seatPref == 'W'? "Fenster" : "Gang";
}
