#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QMessageBox>
#include "TravelAgency.h"
#include "def.hpp"
#include "checkdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionDatei_einlesen_triggered();

    void on_actionBuchungen_anzeigen_triggered();

    void on_tableWidget_cellClicked(int row, int column);

    void on_actionProgramm_beenden_triggered();

    void on_actionFehler_berpr_fen_triggered();

private:
    Ui::MainWindow *ui;
    TravelAgency travelAgency;
};

#endif // MAINWINDOW_H
